#include <cuda_runtime.h>
#include <stdio.h>

__global__ void hello_cuda(){
    printf("hello world, %d\n", threadIdx.x);
}

int main(){
    hello_cuda <<<1, 10>>>();
    cudaDeviceReset();
    return 0;
}