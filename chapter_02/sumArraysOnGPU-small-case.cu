#include <stdio.h>
#include <cuda_runtime.h>

// #define CHECK(call)
// {
//     const cudaError_t error = call;
//     if(error != cudaSuccess){
//         printf("Error: %s:%d", __FILE__, __LINE__);
//         printf("code: %d, reason: %s\n", error, cudaGetErrorString(error));
//     }
// }

void checkResult(float *hostRef, float *gpuRef, const int N){
    double epsilon = 1.0E-8;
    bool match = 1;
    for(int i=0; i<N; i++){
        if(hostRef[i]-gpuRef[i]>epsilon){
            match = 0;
            printf("Arrays do not match!\n");
            printf("host %5.2f gpu %5.2f at current %d \n", hostRef[i], gpuRef[i], i);
        }
    }

    if(match==1){
        printf("Arrays match .\n\n");
    }
}


void initData(float *ip, int size){
    time_t t;
    srand((unsigned) time(&t));

    for(int i=0; i<size; i++){
        ip[i] = (float)(rand()&0xFF)/10.0f;
    }
}

void sumArraysonHost(float *dst, float *src, float *result, const int N){
    for(int i=0; i<N; i++){
        result[i] = src[i] + dst[i];
    }
}

__global__ void sumArrayOnGPU(float *A, float *B, float *C){
    int i = threadIdx.x;
    C[i] = A[i] + B[i];
}

int main(){
    int dev = 0;
    cudaSetDevice(dev);

    int nElem = 32;
    printf("Vector size %d\n", nElem);

    size_t nBytes = nElem * sizeof(float);

    float *h_A, *h_B, *hostRef, *gpuRef;
    h_A = (float *)malloc(nBytes);
    h_B = (float *)malloc(nBytes);
    hostRef = (float *)malloc(nBytes);
    gpuRef = (float *)malloc(nBytes);

    initData(h_A, nElem);
    initData(h_B, nElem);

    memset(h_A, 0, nBytes);
    memset(h_B, 0, nBytes);

    float *d_A, *d_B, *d_C;
    cudaMalloc((float **)&d_A, nBytes);
    cudaMalloc((float **)&d_B, nBytes);
    cudaMalloc((float **)&d_C, nBytes);

    cudaMemcpy(d_A, h_A, nBytes, cudaMemcpyHostToDevice);
    cudaMemcpy(d_B, h_B, nBytes, cudaMemcpyHostToDevice);

    dim3 block(nElem);
    dim3 grid(nElem/block.x);

    sumArrayOnGPU<<<1, 32>>>(d_A, d_B, d_C);
    printf("Execution configuration <<<%d, %d>>>\n", grid.x, block.x);

    cudaMemcpy(gpuRef, d_C, nBytes, cudaMemcpyDeviceToHost);

    sumArraysonHost(h_A, h_B, hostRef, nElem);

    checkResult(hostRef, gpuRef, nElem);

    cudaFree(d_A);
    cudaFree(d_B);
    cudaFree(d_C);

    free(h_A);
    free(h_B);
    free(hostRef);
    free(gpuRef);
    return 0;
}